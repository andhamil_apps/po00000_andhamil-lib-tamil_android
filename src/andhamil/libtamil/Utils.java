/*
 * Utils
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Utils
{
	public static Typeface FONT_TAMIL_BAMINI_NORMAL = null;
	public static Typeface FONT_TAMIL_BAMINI_BOLD = null;
	public static Typeface FONT_TAMIL_BARATHI_NORMAL = null;
	public static Typeface FONT_TAMIL_BARATHI_BOLD = null;
	public static Typeface FONT_TAMIL_KAMBAR_NORMAL = null;
	public static Typeface FONT_TAMIL_KAMBAR_BOLD = null;
	public static Typeface FONT_TAMIL_KAVERI_NORMAL = null;
	public static Typeface FONT_TAMIL_KAVERI_BOLD = null;
	public static Typeface FONT_TAMIL_VALLUVAR_NORMAL = null;
	public static Typeface FONT_TAMIL_VALLUVAR_BOLD = null;

	public static Typeface FONT_TAMIL_NORMAL = null;
	public static Typeface FONT_TAMIL_BOLD = null;
	public static Typeface FONT_ENGLISH = Typeface.SERIF;

	public static String captureScreen(View view, String strCompanayName, String strAppName) throws IOException
	{
		String strExtStorage = Environment.getExternalStorageDirectory().toString();
		strExtStorage += File.separator + strCompanayName;
		File fileDirectory = new File(strExtStorage);
		if (!fileDirectory.exists())
		{
			fileDirectory.mkdir();
		}
		strExtStorage += File.separator + strAppName;
		fileDirectory = new File(strExtStorage);
		if (!fileDirectory.exists())
		{
			fileDirectory.mkdir();
		}
		String strFileName = getCurrentTimeStamp() + ".png";
		File fileImage = new File(fileDirectory.getAbsolutePath(), strFileName);

		try
		{
			if (view != null)
			{
				Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
						Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				view.draw(canvas);
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 40, bytes);
				fileImage.createNewFile();
				FileOutputStream fo = new FileOutputStream(fileImage);
				fo.write(bytes.toByteArray());
				fo.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return fileImage.getAbsolutePath();
	}

	public static String constructHTMLDataTamil(String strData, int nFontSize)
	{
		String strFontFile = "";
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			strFontFile = Konstant.FILENAME_FONT_TAMIL_BAMINI_NORMAL;
		}
		else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BARATHI_NORMAL)
		{
			strFontFile = Konstant.FILENAME_FONT_TAMIL_BARATHI_NORMAL;
		}
		else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_KAMBAR_NORMAL)
		{
			strFontFile = Konstant.FILENAME_FONT_TAMIL_KAMBAR_NORMAL;
		}
		else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_KAVERI_NORMAL)
		{
			strFontFile = Konstant.FILENAME_FONT_TAMIL_KAVERI_NORMAL;
		}
		else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_VALLUVAR_NORMAL)
		{
			strFontFile = Konstant.FILENAME_FONT_TAMIL_VALLUVAR_NORMAL;
		}

		String strReturn = "";
		strReturn += "<HTML>";
		strReturn += "<HEAD>";
		strReturn += "<STYLE TYPE=\"text/css\">";
		strReturn += "@font-face{font-family:'enFont';src:url(\"file:///android_asset/" + strFontFile + "\");}";
		strReturn += "body{font-family:'enFont';font-size:" + nFontSize + "px;text-align:left;}";
		strReturn += "</STYLE>";
		strReturn += "</HEAD>";
		strReturn += "<BODY>";
		strReturn += strData.replace("\n", "<BR>");
		strReturn += "</BODY>";
		strReturn += "</HTML>";
		return strReturn;
	}

	public static String getCurrentTimeStamp()
	{
		String strReturn = "";

		Calendar calendar = Calendar.getInstance();
		strReturn = Integer.toString(calendar.get(Calendar.YEAR));
		strReturn += Integer.toString(calendar.get(Calendar.MONTH));
		strReturn += Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
		strReturn += "_" + Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
		strReturn += Integer.toString(calendar.get(Calendar.MINUTE));
		strReturn += Integer.toString(calendar.get(Calendar.SECOND));

		return strReturn;
	}
	
	public static String getNumberFormat(ArrayList<String> alsData)
	{
		String strFormat = "%01d";
		
		if (alsData.size() > 0 && alsData.size() < 10)
			strFormat = "%01d";
		else if (alsData.size() > 9 && alsData.size() < 100)
			strFormat = "%02d";
		else if (alsData.size() > 99 && alsData.size() < 1000)
			strFormat = "%03d";
		else if (alsData.size() > 9999 && alsData.size() < 10000)
			strFormat = "%04d";
		
		return strFormat;
	}

	public static void loadInstalledApps(Context context)
	{
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		final List<ResolveInfo> listInstalledApps = context.getPackageManager().queryIntentActivities(mainIntent, 0);

		Konstant.INSTALLED_APPS.clear();
		for (ResolveInfo appInfo : listInstalledApps)
		{
			Konstant.INSTALLED_APPS.add(appInfo.activityInfo.packageName);
		}
	}

	public static void shareImage(Activity activity, Context context, String strAppNameWithoutSpace,
			String strAppNameInTamil, String strAppNameInEnglish)
	{
		try
		{
			String strScreenshot = captureScreen(activity.getWindow().getDecorView().getRootView(),
					context.getString(andhamil.libtamil.R.string.company_name_english), strAppNameWithoutSpace);
			Uri uri = Uri.fromFile(new File(strScreenshot));

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("image/jpeg");
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

			if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
				context.startActivity(Intent.createChooser(shareIntent, strAppNameInEnglish));
			else
				context.startActivity(Intent.createChooser(shareIntent, strAppNameInTamil));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void shareText(Context context, String strSubject, String strContent, String strAppNameInTamil,
			String strAppNameInEnglish)
	{
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
		shareIntent.putExtra(Intent.EXTRA_TEXT, strContent);

		if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
			context.startActivity(Intent.createChooser(shareIntent, strAppNameInEnglish));
		else
			context.startActivity(Intent.createChooser(shareIntent, strAppNameInTamil));
		return;
	}

	public static void showToastMsgInTamil(Context context, String strMessage)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			strMessage = EncoderFontBamini.encode(strMessage);
		}
		
		Toast toast = Toast.makeText(context, strMessage, Toast.LENGTH_LONG);
		TextView tvMessage = (TextView) toast.getView().findViewById(android.R.id.message);
		tvMessage.setTypeface(FONT_TAMIL_NORMAL);
		toast.show();
	}

	public static void loadFontFiles(Context context, SharedPreferences preferences)
	{
		Utils.FONT_TAMIL_BAMINI_BOLD = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_BAMINI_BOLD);
		Utils.FONT_TAMIL_BAMINI_NORMAL = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_BAMINI_NORMAL);
		Utils.FONT_TAMIL_BARATHI_BOLD = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_BARATHI_BOLD);
		Utils.FONT_TAMIL_BARATHI_NORMAL = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_BARATHI_NORMAL);
		Utils.FONT_TAMIL_KAMBAR_BOLD = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_KAMBAR_BOLD);
		Utils.FONT_TAMIL_KAMBAR_NORMAL = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_KAMBAR_NORMAL);
		Utils.FONT_TAMIL_KAVERI_BOLD = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_KAVERI_BOLD);
		Utils.FONT_TAMIL_KAVERI_NORMAL = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_KAVERI_NORMAL);
		Utils.FONT_TAMIL_VALLUVAR_BOLD = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_VALLUVAR_BOLD);
		Utils.FONT_TAMIL_VALLUVAR_NORMAL = Typeface.createFromAsset(context.getAssets(),
				Konstant.FILENAME_FONT_TAMIL_VALLUVAR_NORMAL);

		int nFont = preferences.getInt(Konstant.PREF_APP_FONT, Konstant.DEF_APP_FONT);
		switch (nFont)
		{
		case Konstant.ID_FONT_BAMINI:
			Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_BAMINI_BOLD;
			Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_BAMINI_NORMAL;
			break;
		case Konstant.ID_FONT_BARATHI:
			Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_BARATHI_BOLD;
			Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_BARATHI_NORMAL;
			break;
		case Konstant.ID_FONT_KAMBAR:
			Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_KAMBAR_BOLD;
			Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_KAMBAR_NORMAL;
			break;
		case Konstant.ID_FONT_KAVERI:
			Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_KAVERI_BOLD;
			Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_KAVERI_NORMAL;
			break;
		case Konstant.ID_FONT_VALLUVAR:
			Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_VALLUVAR_BOLD;
			Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_VALLUVAR_NORMAL;
			break;
		}
	}
}