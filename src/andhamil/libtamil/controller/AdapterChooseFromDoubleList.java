/*
 * AdapterChooseFromDoubleList
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.controller;

import java.util.ArrayList;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterChooseFromDoubleList extends ArrayAdapter<String>
{
	// Primitives
	private int mnLayoutResourceId;
	private ArrayList<String> mListData;

	// Android
	private Context mContext;

	// Andhamil
	private ListItemHolder mListItemHolder;

	public AdapterChooseFromDoubleList(Context context, int layoutResourceId, ArrayList<String> listData)
	{
		super(context, layoutResourceId, listData);

		mnLayoutResourceId = layoutResourceId;
		mListData = listData;

		mContext = context;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mnLayoutResourceId, parent, false);
			intializeViews(row);
			setFont();
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (ListItemHolder) row.getTag();
		}

		setViewData(position);

		return row;
	}

	static class ListItemHolder
	{
		TextView tvItemData;
	}

	private void intializeViews(View row)
	{
		mListItemHolder = new ListItemHolder();
		mListItemHolder.tvItemData = (TextView) row.findViewById(andhamil.libtamil.R.id.tv_listitem_thalaippu);

		return;
	}

	private void setFont()
	{
		mListItemHolder.tvItemData.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData(final int position)
	{
		String strText = mListData.get(position);
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mListItemHolder.tvItemData.setText(EncoderFontBamini.encode(strText));
		}
		else
		{
			mListItemHolder.tvItemData.setText(strText);
		}

		return;
	}
}