/*
 * AdapterFav
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.controller;

import java.util.ArrayList;
import java.util.Vector;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Konstant;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.view.DialogFav;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterFav extends ArrayAdapter<String>
{
	// Primitives
	private int mnLayoutResourceId;
	private ArrayList<String> mListData;
	private Vector<Integer> mVecListDataIndices;

	// Android
	private Context mContext;
	private SharedPreferences.Editor mPrefEditor;

	// Andhamil
	private DialogFav mDialog;
	private FavouritesListItem mListItemHolder;

	public AdapterFav(Context context, int layoutResourceId, ArrayList<String> listData, Vector<Integer> vecListDataIndices,
			SharedPreferences.Editor prefEditor, DialogFav dialog)
	{
		super(context, layoutResourceId, listData);

		mnLayoutResourceId = layoutResourceId;
		mListData = listData;
		mVecListDataIndices = vecListDataIndices;
		
		mContext = context;		
		mPrefEditor = prefEditor;
		
		mDialog = dialog;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mnLayoutResourceId, parent, false);
			intializeViews(row);
			setFont();
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (FavouritesListItem) row.getTag();
		}

		mListItemHolder.ivFavourite.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mDialog.setAnyFavRemoved(true);

				String strPrefKey = Konstant.PREF_FAV_CHEIYUL + "_" + mVecListDataIndices.get(position);
				mPrefEditor.putBoolean(strPrefKey, false);
				mPrefEditor.commit();

				mListData.remove(position);
				mVecListDataIndices.remove(position);
				notifyDataSetChanged();

				if (mVecListDataIndices.isEmpty())
					mDialog.dismiss();
			}
		});

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mDialog.setUserSelection(mVecListDataIndices.get(position));
				mDialog.setDialogCancelled(false);
				mDialog.dismiss();
			}
		});

		setViewData(position);

		return row;
	}

	static class FavouritesListItem
	{
		ImageView ivFavourite;
		TextView tvFavourite;
	}

	private void intializeViews(View row)
	{
		mListItemHolder = new FavouritesListItem();
		mListItemHolder.tvFavourite = (TextView) row.findViewById(andhamil.libtamil.R.id.tv_listitem_fav);
		mListItemHolder.ivFavourite = (ImageView) row.findViewById(andhamil.libtamil.R.id.iv_listitem_fav);

		return;
	}

	private void setFont()
	{
		mListItemHolder.tvFavourite.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData(final int position)
	{
		String strText = mListData.get(position);
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mListItemHolder.tvFavourite.setText(EncoderFontBamini.encode(strText));
		}
		else
		{
			mListItemHolder.tvFavourite.setText(strText);

		}
		mListItemHolder.ivFavourite.setImageResource(R.drawable.icon_toggle_fav);

		return;
	}
}