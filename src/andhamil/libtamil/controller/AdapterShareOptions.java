/*
 * AdapterShareOptions
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.controller;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import andhamil.libtamil.view.DialogShareOptions;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterShareOptions extends ArrayAdapter<String>
{
	// Primitives
	private int mnLayoutResourceId;
	private String[] mListData;

	// Android
	private Context mContext;

	// Andhamil
	private DialogShareOptions mDialog;
	private ListItemSearchResults mListItemHolder;

	public AdapterShareOptions(Context context, int layoutResourceId, String[] listData, DialogShareOptions dialog)
	{
		super(context, layoutResourceId, listData);

		mnLayoutResourceId = layoutResourceId;
		mListData = listData;

		mContext = context;

		mDialog = dialog;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mnLayoutResourceId, parent, false);
			intializeViews(row);
			setFont();
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (ListItemSearchResults) row.getTag();
		}

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mDialog.setUserSelection(position);
				mDialog.setDialogCancelled(false);
				mDialog.dismiss();
			}
		});

		setViewData(position);

		return row;
	}

	static class ListItemSearchResults
	{
		TextView tvItemData;
	}

	private void intializeViews(View row)
	{
		mListItemHolder = new ListItemSearchResults();
		mListItemHolder.tvItemData = (TextView) row.findViewById(andhamil.libtamil.R.id.tv_listitem_thalaippu);

		return;
	}

	private void setFont()
	{
		mListItemHolder.tvItemData.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData(final int position)
	{
		String strText = mListData[position];
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mListItemHolder.tvItemData.setText(EncoderFontBamini.encode(strText));
		}
		else
		{
			mListItemHolder.tvItemData.setText(strText);
		}
		return;
	}
}