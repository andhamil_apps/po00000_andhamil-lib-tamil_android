/*
 * DialogAbout
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class DialogAbout extends Dialog
{
	// Primitives
	private String mstrNool;
	private String mstrIyatriyavar;
	private String mstrUraiyaasiriyar;
	private String mstrSeyaliUruvakkam;
	private String mstrPangalippaalargal;
	private String mstrNandri;
	private String mstrIntro;
	private String mstrAck;
	
	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private TextView mtvNoolKey;
	private TextView mtvNoolValue;
	private TextView mtvIyatriyavarKey;
	private TextView mtvIyatriyavarValue;
	private TextView mtvUraiyaasiriyarKey;
	private TextView mtvUraiyaasiriyarValue;
	private TextView mtvSeyaliUruvakkamKey;
	private TextView mtvSeyaliUruvakkamValue;
	private TextView mtvPangalippaalargalKey;
	private TextView mtvPangalippaalargalValue;
	private TextView mtvNandriKey;
	private TextView mtvNandriValue;
	private TextView mtvIntro;
	private TextView mtvAck;
	private TextView mtvSocialConnection;
	private ImageButton mibFacebook;
	private ImageButton mibTwitters;
	private ImageButton mibGooglePlus;

	public DialogAbout(Context context)
	{
		super(context);
		mContext = context;

		intializeDataVariables();
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_about);
	}

	private void intializeDataVariables()
	{
		mstrNool = "";
		mstrIyatriyavar = "";
		mstrUraiyaasiriyar = "";
		mstrSeyaliUruvakkam = "";
		mstrPangalippaalargal = "";
		mstrNandri = "";
		mstrIntro = "";
		mstrAck = "";
	}
	
	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_about_title);
		mtvNoolKey = (TextView) findViewById(R.id.tv_about_nool_key);
		mtvNoolValue = (TextView) findViewById(R.id.tv_about_nool_value);
		mtvIyatriyavarKey = (TextView) findViewById(R.id.tv_about_iyatriyavar_key);
		mtvIyatriyavarValue = (TextView) findViewById(R.id.tv_about_iyatriyavar_value);
		mtvUraiyaasiriyarKey = (TextView) findViewById(R.id.tv_about_uraiyaasiriyar_key);
		mtvUraiyaasiriyarValue = (TextView) findViewById(R.id.tv_about_uraiyaasiriyar_value);
		mtvSeyaliUruvakkamKey = (TextView) findViewById(R.id.tv_about_app_development_key);
		mtvSeyaliUruvakkamValue = (TextView) findViewById(R.id.tv_about_app_development_value);
		mtvPangalippaalargalKey = (TextView) findViewById(R.id.tv_about_help_key);
		mtvPangalippaalargalValue = (TextView) findViewById(R.id.tv_about_help_value);
		mtvNandriKey = (TextView) findViewById(R.id.tv_about_courtesy_key);
		mtvNandriValue = (TextView) findViewById(R.id.tv_about_courtesy_value);
		mtvIntro = (TextView) findViewById(R.id.tv_about_intro);
		mtvAck = (TextView) findViewById(R.id.tv_about_acknowledgement);
		mtvSocialConnection = (TextView) findViewById(R.id.tv_about_social_connection);
		mibFacebook = (ImageButton) findViewById(R.id.ib_about_facebook);
		mibTwitters = (ImageButton) findViewById(R.id.ib_about_twitter);
		mibGooglePlus = (ImageButton) findViewById(R.id.ib_about_googleplus);

		return;
	}

	private void validateViews()
	{
		if (mstrNool.isEmpty())
		{
			mtvNoolKey.setVisibility(View.GONE);
			mtvNoolValue.setVisibility(View.GONE);
		}
		else
		{
			mtvNoolKey.setVisibility(View.VISIBLE);
			mtvNoolValue.setVisibility(View.VISIBLE);
		}

		if (mstrIyatriyavar.isEmpty())
		{
			mtvIyatriyavarKey.setVisibility(View.GONE);
			mtvIyatriyavarValue.setVisibility(View.GONE);
		}
		else
		{
			mtvIyatriyavarKey.setVisibility(View.VISIBLE);
			mtvIyatriyavarValue.setVisibility(View.VISIBLE);
		}

		if (mstrUraiyaasiriyar.isEmpty())
		{
			mtvUraiyaasiriyarKey.setVisibility(View.GONE);
			mtvUraiyaasiriyarValue.setVisibility(View.GONE);
		}
		else
		{
			mtvUraiyaasiriyarKey.setVisibility(View.VISIBLE);
			mtvUraiyaasiriyarValue.setVisibility(View.VISIBLE);
		}

		if (mstrPangalippaalargal.isEmpty())
		{
			mtvPangalippaalargalKey.setVisibility(View.GONE);
			mtvPangalippaalargalValue.setVisibility(View.GONE);
		}
		else
		{
			mtvPangalippaalargalKey.setVisibility(View.VISIBLE);
			mtvPangalippaalargalValue.setVisibility(View.VISIBLE);
		}

		if (mstrNandri.isEmpty())
		{
			mtvNandriKey.setVisibility(View.GONE);
			mtvNandriValue.setVisibility(View.GONE);
		}
		else
		{
			mtvNandriKey.setVisibility(View.VISIBLE);
			mtvNandriValue.setVisibility(View.VISIBLE);
		}

		if (mstrIntro.isEmpty())
		{
			mtvIntro.setVisibility(View.GONE);
		}
		else
		{
			mtvIntro.setVisibility(View.VISIBLE);
		}

		if (mstrAck.isEmpty())
		{
			mtvAck.setVisibility(View.GONE);
		}
		else
		{
			mtvAck.setVisibility(View.VISIBLE);
		}
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvNoolKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvNoolValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvIyatriyavarKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvIyatriyavarValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvUraiyaasiriyarKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvUraiyaasiriyarValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvSeyaliUruvakkamKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvSeyaliUruvakkamValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvPangalippaalargalKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvPangalippaalargalValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvNandriKey.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvNandriValue.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvIntro.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvAck.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvSocialConnection.setTypeface(Utils.FONT_TAMIL_NORMAL);
		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.about)));
			mtvNoolKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.nool)));
			mtvNoolValue.setText(EncoderFontBamini.encode(mstrNool));
			mtvIyatriyavarKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.iyatriyavar)));
			mtvIyatriyavarValue.setText(EncoderFontBamini.encode(mstrIyatriyavar));
			mtvUraiyaasiriyarKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.uraiyaasiriyar)));
			mtvUraiyaasiriyarValue.setText(EncoderFontBamini.encode(mstrUraiyaasiriyar));
			mtvSeyaliUruvakkamKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.seyali_uruvakkam)));
			mtvSeyaliUruvakkamValue.setText(EncoderFontBamini.encode(mContext.getString(R.string.company_name_tamil)));
			mtvPangalippaalargalKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.pangalippaalargal)));
			mtvPangalippaalargalValue.setText(EncoderFontBamini.encode(mstrPangalippaalargal));
			mtvNandriKey.setText(EncoderFontBamini.encode(mContext.getString(R.string.nandri)));
			mtvNandriValue.setText(EncoderFontBamini.encode(mstrNandri));
			mtvIntro.setText(EncoderFontBamini.encode(mstrIntro));
			mtvAck.setText(EncoderFontBamini.encode(mstrAck));
			mtvSocialConnection.setText(EncoderFontBamini.encode(mContext.getString(R.string.social_network_prompt)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.about));
			mtvNoolKey.setText(mContext.getString(R.string.nool));
			mtvNoolValue.setText(mstrNool);
			mtvIyatriyavarKey.setText(mContext.getString(R.string.iyatriyavar));
			mtvIyatriyavarValue.setText(mstrIyatriyavar);
			mtvUraiyaasiriyarKey.setText(mContext.getString(R.string.uraiyaasiriyar));
			mtvUraiyaasiriyarValue.setText(mstrUraiyaasiriyar);
			mtvSeyaliUruvakkamKey.setText(mContext.getString(R.string.seyali_uruvakkam));
			mtvSeyaliUruvakkamValue.setText(mContext.getString(R.string.company_name_tamil));
			mtvPangalippaalargalKey.setText(mContext.getString(R.string.pangalippaalargal));
			mtvPangalippaalargalValue.setText(mstrPangalippaalargal);
			mtvNandriKey.setText(mContext.getString(R.string.nandri));
			mtvNandriValue.setText(mstrNandri);
			mtvIntro.setText(mstrIntro);
			mtvAck.setText(mstrAck);
			mtvSocialConnection.setText(mContext.getString(R.string.social_network_prompt));
		}
		return;
	}

	private void addActions()
	{
		mibFacebook.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnFbURL = mContext.getString(R.string.page_link_on_facebook);
				Intent intentPageOnFb = new Intent(Intent.ACTION_VIEW);
				intentPageOnFb.setData(Uri.parse(strPageOnFbURL));
				mContext.startActivity(intentPageOnFb);
			}
		});

		mibTwitters.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnTwitterURL = mContext.getString(R.string.page_link_on_twitter);
				Intent intentPageOnTwitter = new Intent(Intent.ACTION_VIEW);
				intentPageOnTwitter.setData(Uri.parse(strPageOnTwitterURL));
				mContext.startActivity(intentPageOnTwitter);
			}
		});

		mibGooglePlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnGpURL = mContext.getString(R.string.page_link_on_googleplus);
				Intent intentPageOnGp = new Intent(Intent.ACTION_VIEW);
				intentPageOnGp.setData(Uri.parse(strPageOnGpURL));
				mContext.startActivity(intentPageOnGp);
			}
		});

		return;
	}

	public String getNool()
	{
		return mstrNool;
	}

	public void setNool(String strNool)
	{
		this.mstrNool = strNool;
	}

	public String getIyatriyavar()
	{
		return mstrIyatriyavar;
	}

	public void setIyatriyavar(String strIyatriyavar)
	{
		this.mstrIyatriyavar = strIyatriyavar;
	}

	public String getUraiyaasiriyar()
	{
		return mstrUraiyaasiriyar;
	}

	public void setUraiyaasiriyar(String strUraiyaasiriyar)
	{
		this.mstrUraiyaasiriyar = strUraiyaasiriyar;
	}

	public String getSeyaliUruvakkam()
	{
		return mstrSeyaliUruvakkam;
	}

	public void setSeyaliUruvakkam(String strSeyaliUruvakkam)
	{
		this.mstrSeyaliUruvakkam = strSeyaliUruvakkam;
	}

	public String getPangalippaalargal()
	{
		return mstrPangalippaalargal;
	}

	public void setPangalippaalargal(String strPangalippaalargal)
	{
		this.mstrPangalippaalargal = strPangalippaalargal;
	}

	public String getNandri()
	{
		return mstrNandri;
	}

	public void setNandri(String strNandri)
	{
		this.mstrNandri = strNandri;
	}

	public String getIntro()
	{
		return mstrIntro;
	}

	public void setIntro(String strIntro)
	{
		this.mstrIntro = strIntro;
	}

	public String getAck()
	{
		return mstrAck;
	}

	public void setAck(String strAck)
	{
		this.mstrAck = strAck;
	}
	
	public void displayDialog()
	{
		intializeViews();
		validateViews();
		setFont();
		setViewData();
		addActions();

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}
}