/*
 * DialogChooseFromDoubleList
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterChooseFromDoubleList;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class DialogChooseFromDoubleList extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvList;

	public DialogChooseFromDoubleList(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_choose_fsl);
	}

	public void displayDialogForPrimary(String strPrimaryTitle, final String strSecondaryTitle,
			final ArrayList<String> alsPrimaryData, final ArrayList<String> alsSecondaryData,
			final ArrayList<Integer> alnPrimaryIndices, final ArrayList<Integer> alnSecondaryIndices)
	{
		intializeViews();
		setFont();
		setViewData(strPrimaryTitle);
		addActionsForPrimary(strSecondaryTitle, alsPrimaryData, alsSecondaryData, alnPrimaryIndices, alnSecondaryIndices);

		AdapterChooseFromDoubleList adapterChooseFromDoubleList = new AdapterChooseFromDoubleList(mContext,
				R.layout.listitem_thalaippu, alsPrimaryData);
		mlvList.setAdapter(adapterChooseFromDoubleList);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void displayDialogForSecondary(String strSecondaryTitle, ArrayList<String> alsSecondaryData,
			final ArrayList<Integer> alnPrimaryIndices, final ArrayList<Integer> alnSecondaryIndices,
			final int nPrimaryPosition)
	{
		intializeViews();
		setFont();
		setViewData(strSecondaryTitle);

		ArrayList<String> alsSecondaryDataSubset = new ArrayList<String>();
		int nStartIndex = 0;
		if (nPrimaryPosition > 0)
			nStartIndex = alnPrimaryIndices.get(nPrimaryPosition - 1);

		int nEndIndex = alnPrimaryIndices.get(nPrimaryPosition);

		for (int i = nStartIndex; i < nEndIndex; i++)
		{
			alsSecondaryDataSubset.add(alsSecondaryData.get(i));
		}
		int nPreviousIndex = nStartIndex;
		addActionsForSecondary(nPreviousIndex, alnSecondaryIndices);

		AdapterChooseFromDoubleList adapterChooseFromDoubleList = new AdapterChooseFromDoubleList(mContext,
				R.layout.listitem_thalaippu, alsSecondaryDataSubset);
		mlvList.setAdapter(adapterChooseFromDoubleList);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_choose_fsl_title);
		mlvList = (ListView) findViewById(R.id.lv_choose_fsl_content);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData(String strTitle)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(strTitle));
		}
		else
		{
			mtvTitle.setText(strTitle);
		}

		return;
	}

	private void addActionsForPrimary(final String strSecondaryTitle, final ArrayList<String> alsPrimaryData,
			final ArrayList<String> alsSecondaryData, final ArrayList<Integer> alnPrimaryIndices,
			final ArrayList<Integer> alnSecondaryIndices)
	{
		mlvList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, final int nPosition, long id)
			{
				final DialogChooseFromDoubleList dialogChooseL2 = new DialogChooseFromDoubleList(mContext);
				dialogChooseL2.displayDialogForSecondary(alsPrimaryData.get(nPosition) + " " + strSecondaryTitle, alsSecondaryData,
						alnPrimaryIndices, alnSecondaryIndices, nPosition);
				dialogChooseL2.setOnDismissListener(new OnDismissListener()
				{
					@Override
					public void onDismiss(DialogInterface dialog)
					{
						if (!dialogChooseL2.isDialogCancelled())
						{
							mnUserSelection = dialogChooseL2.getUserSelection();
							mbDialogCancelled = false;
						}
						DialogChooseFromDoubleList.this.dismiss();
					}
				});
			}
		});

		return;
	}

	private void addActionsForSecondary(final int nPreviousIndex, final ArrayList<Integer> alnSecondaryIndices)
	{
		mlvList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int nPosition, long id)
			{
				int nSecondaryIndex = nPreviousIndex + nPosition;
				if (nSecondaryIndex > 0)
				{
					mnUserSelection = alnSecondaryIndices.get(nSecondaryIndex - 1);
				}
				else
				{
					mnUserSelection = 0;
				}
				mbDialogCancelled = false;
				DialogChooseFromDoubleList.this.dismiss();
			}
		});

		return;
	}
}