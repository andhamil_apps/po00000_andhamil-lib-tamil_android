/*
 * DialogChooseFromSimpleList
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterChooseFromSimpleList;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

public class DialogChooseFromSimpleList extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvList;

	public DialogChooseFromSimpleList(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_choose_fsl);
	}

	public void displayDialog(ArrayList<String> alsData, String strTitle)
	{
		intializeViews();
		setFont();
		setViewData(strTitle);

		AdapterChooseFromSimpleList adapterChooseFromSimpleList = new AdapterChooseFromSimpleList(mContext,
				andhamil.libtamil.R.layout.listitem_poem, alsData, DialogChooseFromSimpleList.this);
		mlvList.setAdapter(adapterChooseFromSimpleList);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void setDialogCancelled(boolean bDialogCancelled)
	{
		mbDialogCancelled = bDialogCancelled;
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public void setUserSelection(int nUserSelection)
	{
		mnUserSelection = nUserSelection;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_choose_fsl_title);
		mlvList = (ListView) findViewById(R.id.lv_choose_fsl_content);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData(String strTitle)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(strTitle));
		}
		else
		{
			mtvTitle.setText(strTitle);
		}

		return;
	}
}