/*
 * DialogChooseFromSingleList
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterChooseFromSingleList;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class DialogChooseFromSingleList extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvList;

	public DialogChooseFromSingleList(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_choose_fsl);
	}

	public void displayDialog(String strTitle, ArrayList<String> alsData, final ArrayList<Integer> alnIndices)
	{
		intializeViews();
		setFont();
		setViewData(strTitle);
		addActions(alnIndices);

		AdapterChooseFromSingleList adapterChooseFromSingleList = new AdapterChooseFromSingleList(mContext,
				andhamil.libtamil.R.layout.listitem_poem, alsData);
		mlvList.setAdapter(adapterChooseFromSingleList);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_choose_fsl_title);
		mlvList = (ListView) findViewById(R.id.lv_choose_fsl_content);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData(String strTitle)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(strTitle));
		}
		else
		{
			mtvTitle.setText(strTitle);
		}

		return;
	}

	private void addActions(final ArrayList<Integer> alnIndices)
	{
		mlvList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int nPosition, long id)
			{
				if (nPosition <= 0)
				{
					mnUserSelection = 0;
				}
				else
				{
					mnUserSelection = alnIndices.get(nPosition - 1);
				}
				mbDialogCancelled = false;

				DialogChooseFromSingleList.this.dismiss();
			}
		});

		return;
	}
}