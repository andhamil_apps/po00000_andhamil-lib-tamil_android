/*
 * DialogFav
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;
import java.util.Vector;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterFav;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

public class DialogFav extends Dialog
{
	// Primitives
	private boolean mbAnyFavRemoved;
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvFav;

	public DialogFav(Context context)
	{
		super(context);

		mContext = context;

		mbAnyFavRemoved = false;
		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_fav);
	}

	public void displayDialog(ArrayList<String> listData, Vector<Integer> vecListDataIndices,
			final SharedPreferences.Editor prefEditor)
	{

		intializeViews();
		setFont();
		setViewData();

		AdapterFav favouritesAdapter = new AdapterFav(mContext, andhamil.libtamil.R.layout.listitem_fav, listData,
				vecListDataIndices, prefEditor, this);
		mlvFav.setAdapter(favouritesAdapter);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void setAnyFavRemoved(boolean bAnyFavRemoved)
	{
		mbAnyFavRemoved = bAnyFavRemoved;
	}

	public boolean isAnyFavRemoved()
	{
		return mbAnyFavRemoved;
	}

	public void setDialogCancelled(boolean bDialogCancelled)
	{
		mbDialogCancelled = bDialogCancelled;
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public void setUserSelection(int nUserSelection)
	{
		mnUserSelection = nUserSelection;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_favourites_title);
		mlvFav = (ListView) findViewById(R.id.lv_favourites);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.favourites)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.favourites));
		}
	}
}