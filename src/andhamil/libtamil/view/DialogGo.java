/*
 * DialogGo
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2023.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

public class DialogGo extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserInput;

	// Android
	private Context mContext;

	// UI Controls
	private TextView tvTitle;
	private TextView tvNumber;
	private TextView tvGo;
	private EditText etNumber;

	public DialogGo(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserInput = 0;

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_go);
	}

	public void displayDialog(final int nCheiyulSize, String strCheiyulTypeSingular, final String strCheiyulTypePlural)
	{
		intializeViews();
		setFont();
		setViewData(strCheiyulTypeSingular);
		addActions(nCheiyulSize, strCheiyulTypeSingular, strCheiyulTypePlural);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public int getUserInput()
	{
		return mnUserInput;
	}

	private void intializeViews()
	{
		tvTitle = (TextView) findViewById(R.id.tv_go_title);
		tvNumber = (TextView) findViewById(R.id.tv_go_number);
		tvGo = (TextView) findViewById(R.id.tv_go_go);
		etNumber = (EditText) findViewById(R.id.et_go_number);

		return;
	}

	private void setFont()
	{
		tvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		tvNumber.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvGo.setTypeface(Utils.FONT_TAMIL_NORMAL);
		etNumber.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData(String strCheiyulTypeSingular)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			tvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.go)));
			tvNumber.setText(
					EncoderFontBamini.encode(strCheiyulTypeSingular + " " + mContext.getString(R.string.number)));
			tvGo.setText(EncoderFontBamini.encode(mContext.getString(R.string.go)));
		}
		else
		{
			tvTitle.setText(mContext.getString(R.string.go));
			tvNumber.setText(strCheiyulTypeSingular + " " + mContext.getString(R.string.number));
			tvGo.setText(mContext.getString(R.string.go));
		}

		return;
	}

	private void addActions(final int nCheiyulSize, String strCheiyulTypeSingular, final String strCheiyulTypePlural)
	{
		tvGo.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				try
				{
					mnUserInput = Integer.parseInt(etNumber.getText().toString());

					if ((mnUserInput < 1) || (mnUserInput > nCheiyulSize))
						Utils.showToastMsgInTamil(mContext, nCheiyulSize + " " + strCheiyulTypePlural + " "
								+ mContext.getString(R.string.msg_max_number_exceeded));
					else
						mbDialogCancelled = false;

					DialogGo.this.dismiss();
				}
				catch (Exception e)
				{
					Utils.showToastMsgInTamil(mContext, mContext.getString(R.string.msg_invalid_entry));
					DialogGo.this.cancel();
				}
				return;
			}
		});

		return;
	}
}