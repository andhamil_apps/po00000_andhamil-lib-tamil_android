/*
 * DialogInstallAndhamilBooks
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2023.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogInstallAndhamilBooks extends Dialog
{
	private Context mContext;
	private SharedPreferences mPreferences;

	public DialogInstallAndhamilBooks(Context context, SharedPreferences preferences)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_install_andhamil_books);
		this.setCancelable(false);

		mContext = context;
		mPreferences = preferences;

		Utils.loadFontFiles(mContext, mPreferences);
	}

	public void displayDialog()
	{
		TextView tvTitle = (TextView) findViewById(R.id.tv_iab_title);
		tvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		tvTitle.setText(mContext.getString(R.string.andhamil_noolgal));

		TextView tvRatePrompt = (TextView) findViewById(R.id.tv_iab_text);
		tvRatePrompt.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvRatePrompt.setText(mContext.getString(R.string.msg_install_andhamil_books));

		TextView tvAppstore = (TextView) findViewById(R.id.tv_iab_appstore);
		tvAppstore.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvAppstore.setText(mContext.getString(R.string.google_play_store));
		tvAppstore.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogInstallAndhamilBooks.this.dismiss();
				Intent intentAppOnGooglePlay = new Intent(Intent.ACTION_VIEW);
				intentAppOnGooglePlay.setData(Uri.parse(mContext.getString(R.string.andhamil_books_app_link_on_google_play_store)));
				mContext.startActivity(intentAppOnGooglePlay);
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}
}