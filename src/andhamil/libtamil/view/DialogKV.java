/*
 * DialogKV
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogKV extends Dialog
{
	// Primitives
	private String mstrUraiKey1;
	private String mstrUraiValue1;
	private String mstrUraiKey2;
	private String mstrUraiValue2;
	private String mstrUraiKey3;
	private String mstrUraiValue3;
	private String mstrUraiKey4;
	private String mstrUraiValue4;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private TextView mtvCheiyul;
	private TextView mtvUraiKey1;
	private TextView mtvUraiValue1;
	private TextView mtvUraiKey2;
	private TextView mtvUraiValue2;
	private TextView mtvUraiKey3;
	private TextView mtvUraiValue3;
	private TextView mtvUraiKey4;
	private TextView mtvUraiValue4;

	public DialogKV(Context context)
	{
		super(context);

		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_kv);

		mstrUraiKey1 = "";
		mstrUraiValue1 = "";
		mstrUraiKey2 = "";
		mstrUraiValue2 = "";
		mstrUraiKey3 = "";
		mstrUraiValue3 = "";
		mstrUraiKey4 = "";
		mstrUraiValue4 = "";
	}

	public void setUrai1(String strUraiKey1, String strUraiValue1)
	{
		mstrUraiKey1 = strUraiKey1;
		mstrUraiValue1 = strUraiValue1;
	}

	public void setUrai2(String strUraiKey2, String strUraiValue2)
	{
		mstrUraiKey2 = strUraiKey2;
		mstrUraiValue2 = strUraiValue2;
	}

	public void setUrai3(String strUraiKey3, String strUraiValue3)
	{
		mstrUraiKey3 = strUraiKey3;
		mstrUraiValue3 = strUraiValue3;
	}

	public void setUrai4(String strUraiKey4, String strUraiValue4)
	{
		mstrUraiKey4 = strUraiKey4;
		mstrUraiValue4 = strUraiValue4;
	}

	public void displayDialog(String strTitle, String strCheiyul)
	{
		intializeViews();
		setFont();
		setFontSize();
		setViewData(strTitle, strCheiyul);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.FLAG_FULLSCREEN;
		getWindow().setAttributes(lp);
		show();
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_kv_title);
		mtvCheiyul = (TextView) findViewById(R.id.tv_kv_cheiyul);
		mtvUraiKey1 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_1_key);
		mtvUraiValue1 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_1_value);
		mtvUraiKey2 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_2_key);
		mtvUraiValue2 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_2_value);
		mtvUraiKey3 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_3_key);
		mtvUraiValue3 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_3_value);
		mtvUraiKey4 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_4_key);
		mtvUraiValue4 = (TextView) findViewById(R.id.tv_kv_cheiyul_urai_4_value);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvCheiyul.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvUraiKey1.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvUraiValue1.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvUraiKey2.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvUraiValue2.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvUraiKey3.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvUraiValue3.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvUraiKey4.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvUraiValue4.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setFontSize()
	{
		mtvTitle.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvCheiyul.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mtvUraiKey1.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvUraiValue1.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mtvUraiKey2.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvUraiValue2.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mtvUraiKey3.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvUraiValue3.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mtvUraiKey3.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvUraiValue4.setTextSize(mContext.getResources().getInteger(R.integer.font_m));

		return;
	}

	private void setViewData(String strTitle, String strCheiyul)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			mtvTitle.setText(EncoderFontBamini.encode(strTitle));
		else
			mtvTitle.setText(strTitle);

		if (strCheiyul.isEmpty())
		{
			mtvCheiyul.setVisibility(View.GONE);
		}
		else
		{
			mtvCheiyul.setVisibility(View.VISIBLE);
			mtvCheiyul.setTypeface(Utils.FONT_TAMIL_NORMAL);
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
				mtvCheiyul.setText(EncoderFontBamini.encode(strCheiyul));
			else
				mtvCheiyul.setText(strCheiyul);
		}

		if (mstrUraiValue1.isEmpty())
		{
			mtvUraiKey1.setVisibility(View.GONE);
			mtvUraiValue1.setVisibility(View.GONE);
		}
		else
		{
			mtvUraiKey1.setVisibility(View.VISIBLE);
			mtvUraiKey1.setTypeface(Utils.FONT_TAMIL_NORMAL);
			mtvUraiValue1.setVisibility(View.VISIBLE);
			mtvUraiValue1.setTypeface(Utils.FONT_TAMIL_NORMAL);
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			{
				mtvUraiKey1.setText(EncoderFontBamini.encode(mstrUraiKey1));
				mtvUraiValue1.setText(EncoderFontBamini.encode(mstrUraiValue1.replace(", ", ",\n")));
			}
			else
			{
				mtvUraiKey1.setText(mstrUraiKey1);
				mtvUraiValue1.setText(mstrUraiValue1.replace(", ", ",\n"));
			}
		}

		if (mstrUraiValue2.isEmpty())
		{
			mtvUraiKey2.setVisibility(View.GONE);
			mtvUraiValue2.setVisibility(View.GONE);
		}
		else
		{
			mtvUraiKey2.setVisibility(View.VISIBLE);
			mtvUraiKey2.setTypeface(Utils.FONT_TAMIL_NORMAL);
			mtvUraiValue2.setVisibility(View.VISIBLE);
			mtvUraiValue2.setTypeface(Utils.FONT_TAMIL_NORMAL);
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			{
				mtvUraiKey2.setText(EncoderFontBamini.encode(mstrUraiKey2));
				mtvUraiValue2.setText(EncoderFontBamini.encode(mstrUraiValue2.replace(", ", ",\n")));
			}
			else
			{
				mtvUraiKey2.setText(mstrUraiKey2);
				mtvUraiValue2.setText(mstrUraiValue2.replace(", ", ",\n"));
			}
		}

		if (mstrUraiValue3.isEmpty())
		{
			mtvUraiKey3.setVisibility(View.GONE);
			mtvUraiValue3.setVisibility(View.GONE);
		}
		else
		{
			mtvUraiKey3.setVisibility(View.VISIBLE);
			mtvUraiKey3.setTypeface(Utils.FONT_TAMIL_NORMAL);
			mtvUraiValue3.setVisibility(View.VISIBLE);
			mtvUraiValue3.setTypeface(Utils.FONT_TAMIL_NORMAL);
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			{
				mtvUraiKey3.setText(EncoderFontBamini.encode(mstrUraiKey3));
				mtvUraiValue3.setText(EncoderFontBamini.encode(mstrUraiValue3.replace(", ", ",\n")));
			}
			else
			{
				mtvUraiKey3.setText(mstrUraiKey3);
				mtvUraiValue3.setText(mstrUraiValue3.replace(", ", ",\n"));
			}
		}

		if (mstrUraiValue4.isEmpty())
		{
			mtvUraiKey4.setVisibility(View.GONE);
			mtvUraiValue4.setVisibility(View.GONE);
		}
		else
		{
			mtvUraiKey4.setVisibility(View.VISIBLE);
			mtvUraiKey4.setTypeface(Utils.FONT_TAMIL_NORMAL);
			mtvUraiValue4.setVisibility(View.VISIBLE);
			mtvUraiValue4.setTypeface(Utils.FONT_TAMIL_NORMAL);
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			{
				mtvUraiKey4.setText(EncoderFontBamini.encode(mstrUraiKey4));
				mtvUraiValue4.setText(EncoderFontBamini.encode(mstrUraiValue4.replace(", ", ",\n")));
			}
			else
			{
				mtvUraiKey4.setText(mstrUraiKey4);
				mtvUraiValue4.setText(mstrUraiValue4.replace(", ", ",\n"));
			}
		}
	}
}
