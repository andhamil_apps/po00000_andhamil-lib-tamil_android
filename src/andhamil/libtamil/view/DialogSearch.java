/*
 * DialogSearch
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Vector;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class DialogSearch extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView tvTitle;
	private TextView tvSearchTerms;
	private TextView tvSearch;
	private EditText etSearchTerms;
	private RadioGroup rgSearchOptions;
	private RadioButton rbSearchAll;
	private RadioButton rbSearchAny;

	public DialogSearch(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_search);
	}

	public void displayDialog(final ArrayList<String> alsDataPool, final ArrayList<String> alsDataToDisplay)
	{
		intializeViews();
		setFont();
		setViewData();
		addActions(alsDataPool, alsDataToDisplay);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		tvTitle = (TextView) findViewById(R.id.tv_search_title);
		tvSearchTerms = (TextView) findViewById(R.id.tv_search_terms);
		tvSearch = (TextView) findViewById(R.id.tv_search_search);
		etSearchTerms = (EditText) findViewById(R.id.et_search_terms);
		rgSearchOptions = (RadioGroup) findViewById(R.id.rg_search_choice);
		rbSearchAll = (RadioButton) findViewById(R.id.rb_search_all);
		rbSearchAny = (RadioButton) findViewById(R.id.rb_search_any);

		return;
	}

	private void setFont()
	{
		tvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		tvSearchTerms.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvSearch.setTypeface(Utils.FONT_TAMIL_NORMAL);
		etSearchTerms.setTypeface(Utils.FONT_TAMIL_NORMAL);
		rbSearchAll.setTypeface(Utils.FONT_TAMIL_NORMAL);
		rbSearchAny.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			tvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.thaedal)));
			tvSearchTerms.setText(EncoderFontBamini.encode(mContext.getString(R.string.thaedal_sorkal)));
			rbSearchAll.setText(EncoderFontBamini.encode(mContext.getString(R.string.anaiththum)));
			rbSearchAny.setText(EncoderFontBamini.encode(mContext.getString(R.string.aedhaenum)));
			tvSearch.setText(EncoderFontBamini.encode(mContext.getString(R.string.thaeduga)));
		}
		else
		{
			tvTitle.setText(mContext.getString(R.string.thaedal));
			tvSearchTerms.setText(mContext.getString(R.string.thaedal_sorkal));
			rbSearchAll.setText(mContext.getString(R.string.anaiththum));
			rbSearchAny.setText(mContext.getString(R.string.aedhaenum));
			tvSearch.setText(mContext.getString(R.string.thaeduga));
		}

		return;
	}

	private void addActions(final ArrayList<String> alsDataPool, final ArrayList<String> alsDataToDisplay)
	{
		tvSearch.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				try
				{
					String strUserInput = etSearchTerms.getText().toString().trim().toLowerCase(Locale.CANADA);
					if (strUserInput.equals(""))
					{
						Utils.showToastMsgInTamil(mContext, mContext.getString(R.string.msg_invalid_input));
						DialogSearch.this.cancel();
					}
					else
					{
						ArrayList<String> listSearchResults = new ArrayList<String>();
						Vector<Integer> vecSearchResultsKuralNo = new Vector<Integer>();

						if (rgSearchOptions.getCheckedRadioButtonId() == R.id.rb_search_all)
						{
							String[] arraySearchAllTerms = strUserInput.split(" ");
							for (int i = 0; i < alsDataPool.size(); i++)
							{
								String[] strarrayKural = alsDataPool.get(i).replaceAll("\\p{Punct}", "").split("\\s+");

								ArrayList<String> alsKural = new ArrayList<String>();
								for (int j = 0; j < strarrayKural.length; j++)
									alsKural.add(strarrayKural[j]);

								boolean bSuccess = true;
								for (int k = 0; k < arraySearchAllTerms.length; k++)
								{
									if (!alsKural.contains(arraySearchAllTerms[k]))
									{
										bSuccess = false;
										break;
									}
								}

								if (bSuccess)
								{
									if (!vecSearchResultsKuralNo.contains(i))
									{
										listSearchResults.add(alsDataToDisplay.get(i));
										vecSearchResultsKuralNo.add(i);
									}
								}
							}
						}
						else if (rgSearchOptions.getCheckedRadioButtonId() == R.id.rb_search_any)
						{
							String[] arraySearchAnyTerms = strUserInput.split(" ");
							for (int i = 0; i < alsDataPool.size(); i++)
							{
								for (int j = 0; j < arraySearchAnyTerms.length; j++)
								{
									String[] strarrayKural = alsDataPool.get(i).replaceAll("\\p{Punct}", "")
											.split("\\s+");
									for (int k = 0; k < strarrayKural.length; k++)
									{
										if (arraySearchAnyTerms[j].equals(strarrayKural[k]))
										{
											if (!vecSearchResultsKuralNo.contains(i))
											{
												listSearchResults.add(alsDataToDisplay.get(i));
												vecSearchResultsKuralNo.add(i);
											}
											break;
										}

									}
								}
							}
						}

						final DialogSearchResults dialogSearchResults = new DialogSearchResults(mContext);
						dialogSearchResults.displayDialog(listSearchResults, vecSearchResultsKuralNo);
						dialogSearchResults.setOnDismissListener(new OnDismissListener()
						{
							@Override
							public void onDismiss(DialogInterface dialog)
							{
								mnUserSelection = dialogSearchResults.getUserSelection();
								mbDialogCancelled = false;
								DialogSearch.this.dismiss();
							}
						});
					}
				}
				catch (Exception e)
				{
					Utils.showToastMsgInTamil(mContext, mContext.getString(R.string.msg_invalid_input));
					DialogSearch.this.cancel();
				}

				return;
			}
		});

		return;
	}
}