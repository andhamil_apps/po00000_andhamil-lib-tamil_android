/*
 * DialogSearchResults
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2023.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import java.util.ArrayList;
import java.util.Vector;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterSearchResults;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

public class DialogSearchResults extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvSearchResults;

	public DialogSearchResults(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_search_results);
	}

	public void displayDialog(ArrayList<String> listData, final Vector<Integer> vecListDataIndices)
	{
		if (listData.isEmpty())
		{
			Utils.showToastMsgInTamil(mContext, mContext.getString(R.string.msg_search_result_empty));
			return;
		}

		intializeViews();
		setFont();
		setViewData();

		AdapterSearchResults searchResultsAdapter = new AdapterSearchResults(mContext,
				andhamil.libtamil.R.layout.listitem_poem, listData, vecListDataIndices, this);
		mlvSearchResults.setAdapter(searchResultsAdapter);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void setDialogCancelled(boolean bDialogCancelled)
	{
		mbDialogCancelled = bDialogCancelled;
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public void setUserSelection(int nUserSelection)
	{
		mnUserSelection = nUserSelection;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_search_results_title);
		mlvSearchResults = (ListView) findViewById(R.id.lv_search_results);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.thaedal_mudivugal)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.thaedal_mudivugal));
		}
	}
}