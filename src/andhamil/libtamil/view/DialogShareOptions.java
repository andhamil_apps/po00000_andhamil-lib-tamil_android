/*
 * DialogShareOptions
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import andhamil.libtamil.controller.AdapterShareOptions;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

public class DialogShareOptions extends Dialog
{
	// Primitives
	private boolean mbDialogCancelled;
	private int mnUserSelection;

	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private ListView mlvShareOptions;

	public DialogShareOptions(Context context)
	{
		super(context);

		mContext = context;

		mbDialogCancelled = true;
		mnUserSelection = 0;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_share);
	}

	public void displayDialog()
	{
		intializeViews();
		setFont();
		setViewData();

		AdapterShareOptions searchResultsAdapter = new AdapterShareOptions(mContext,
				andhamil.libtamil.R.layout.listitem_thalaippu,
				mContext.getResources().getStringArray(R.array.share_options), DialogShareOptions.this);
		mlvShareOptions.setAdapter(searchResultsAdapter);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void setDialogCancelled(boolean bDialogCancelled)
	{
		mbDialogCancelled = bDialogCancelled;
	}

	public boolean isDialogCancelled()
	{
		return mbDialogCancelled;
	}

	public void setUserSelection(int nUserSelection)
	{
		mnUserSelection = nUserSelection;
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_share_title);
		mlvShareOptions = (ListView) findViewById(R.id.lv_share_content);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.share)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.share));
		}
	}
}