/*
 * DialogSingleTextView
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogSingleTextView extends Dialog
{
	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private TextView mtvData;

	public DialogSingleTextView(Context context)
	{
		super(context);

		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_singletextview);
	}

	public void displayDialog(String strTitle, String strData)
	{
		intializeViews();
		setFont();
		setFontSize();
		setViewData(strTitle, strData);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_singletv_title);
		mtvData = (TextView) findViewById(R.id.tv_singletv_maindata);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvData.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setFontSize()
	{
		mtvTitle.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvData.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		return;
	}

	private void setViewData(String strTitle, String strData)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(strTitle));
			mtvData.setText(EncoderFontBamini.encode(strData));
		}
		else
		{
			mtvTitle.setText(strTitle);
			mtvData.setText(strData);
		}
	}
}