/*
 * EnTextView
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.libtamil.Konstant;
import andhamil.libtamil.R;
import andhamil.libtamil.Utils;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class EnTextView extends TextView
{
	private static final String TAG = "TextView";

	public EnTextView(Context context)
	{
		super(context);
	}

	public EnTextView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setCustomFont(context, attrs);
	}

	public EnTextView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);
	}

	private void setCustomFont(Context ctx, AttributeSet attrs)
	{
		TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EnTextView);
		String customFont = a.getString(R.styleable.EnTextView_customFont);
		setCustomFont(ctx, customFont);
		a.recycle();
	}

	public boolean setCustomFont(Context ctx, String asset)
	{
		Typeface tf = null;
		try
		{
			if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			{
				tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_TAMIL_BAMINI_NORMAL);
			}
			else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BARATHI_NORMAL)
			{
				tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_TAMIL_BARATHI_NORMAL);
			}
			else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_KAMBAR_NORMAL)
			{
				tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_TAMIL_KAMBAR_NORMAL);
			}
			else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_KAVERI_NORMAL)
			{
				tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_TAMIL_KAVERI_NORMAL);
			}
			else if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_VALLUVAR_NORMAL)
			{
				tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_TAMIL_VALLUVAR_NORMAL);
			}
		}
		catch (Exception e)
		{
			Log.e(TAG, "Could not get typeface: " + e.getMessage());
			return false;
		}

		setTypeface(tf);
		return true;
	}
}